const slides = document.querySelectorAll('.slide');

for (const slide of slides) {
    slide.addEventListener('click', () => {
        cleaerActiveClasses();
        slide.classList.add('active');
    })
}

function cleaerActiveClasses() {
    slides.forEach((slide) => {
        slide.classList.remove('active');
    })
}